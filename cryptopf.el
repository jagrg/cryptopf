;;; cryptopf.el --- Cryptocurrency portfolio tracker -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2018 Jonathan Gregory

;; Author: Jonathan Gregory <jgrg at autistici dot org>
;; URL: https://gitlab.com/jagrg/cryptopf
;; Version: 0.3
;; Package-Requires: ((emacs "24.4") (dash "2.11.0") (ledger-mode "3.0.0"))

;; This file is not part of GNU Emacs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This is a cryptocurrency portfolio tracker for Emacs using the
;; cryptocompare (https://www.cryptocompare.com/) API.

;; The simplest way to start using this program is to define
;; `crypto-holdings' and `crypto-entry-price'. However, depending on
;; the number of transactions it may be easier to add them to a
;; separate file using ledger's textual format (see
;; https://www.ledger-cli.org/).

;; 2017/04/16 Bought Bitcoin
;;     Assets:Bitcoin Wallet                  0.00000003 BTC @ $1,226.18
;;     Assets:Cash

;; You will then have to assign this file to the `crypto-ledger-file'
;; variable. Once you have a few transactions added, run the
;; interactive command `cryptopf', and results will pop up in a new
;; buffer below. To check the price per coin, move the cursor to the
;; corresponding coin using n or p. This also checks the price and
;; percent change in the last 24 hours. When you are done, press q to
;; quit the buffer or g to update.

;; To track other currencies and commodities not supported by the API,
;; you will need to specify what the current price is for each
;; commodity in the price history file (defaults to ~/.pricedb). In
;; most cases, the best way to do this is to have your price download
;; script maintain this file. Check the ledger documentation for more
;; information.

;; Disclaimer: The crypto market is very volatile, so only invest what
;; you can afford to lose. I am also not liable for the accuracy of
;; this program.

;;; Code:

(require 'dash)
(require 'url)
(require 'url-cache)
(require 'align)
(require 'json)
(require 'ledger-mode)
(require 'tabulated-list)
(eval-when-compile (require 'subr-x))

(defgroup cryptopf nil
  "Customization group for `cryptopf'."
  :group 'crypto
  :prefix "crypto-")

(defconst crypto-buffer-name "*portfolio*")

(defcustom crypto-ledger-file nil
  "File name of the ledger file/journal.
You may include any asset to your portfolio, even those not supported
by the API. However, any non-crypto asset present in the journal
should also be present in the pricedb file."
  :type 'file)

(defcustom crypto-default-currency-code "USD"
  "Default currency code."
  :type 'string)

(defcustom crypto-default-currency-sign "$"
  "Default currency symbol."
  :type 'string)

(defcustom crypto-holdings nil
  "List of symbol/amount pairs.
The entries in this list are cons cells where the car is the
currency's symbol and cdr is the number of coins."
  :type '(repeat (cons (string :tag "Symbol")
		       (number :tag "Amount"))))

(defcustom crypto-entry-price nil
  "List of symbol/price pairs.
The entries in this list are cons cells where the car is the coin's
symbol and the cdr is the entry price."
  :type '(repeat (cons (string :tag "Symbol")
		       (number :tag "Price"))))

(defcustom crypto-pricedb-file "~/.pricedb"
  "Path to the price database file."
  :type 'file)

(defcustom crypto-ignore nil
  "List of assets to ignore."
  :type '(repeat string :tag "Symbol"))

(defcustom crypto-use-header-line t
  "Disable the header line when nil."
  :type '(choice (const :tag "Disable the header-line" nil)
		 (const :tag "Enable the header-line" t)))

(defcustom crypto-use-mode-line nil
  "Disable the mode line when nil."
  :type '(choice (const :tag "Disable the mode-line" nil)
		 (const :tag "Enable the mode-line" t)))

(defface crypto-positive-number-face
  '((t (:foreground "green")))
  "Face used for positive numbers.")

(defface crypto-negative-number-face
  '((t (:foreground "red")))
  "Face used for negative numbers.")

(define-derived-mode crypto-mode tabulated-list-mode "crypto-mode"
  "Major mode for tracking portfolio."
  (use-local-map crypto-mode-map)
  (setq tabulated-list-format
	;; These are col names, followed by col width and sort arg.
	[("Asset" 10 nil)
    	 ("Value" 13 nil)
    	 ("Amount" 14 nil)
    	 ("Weight" 10 t)
    	 ("Profit" 9 t)]
	;; Sort by weight value
	tabulated-list-sort-key (cons "Weight" t))
  ;; Use header-line?
  (unless crypto-use-header-line
    (setq tabulated-list-use-header-line nil))
  ;; Use mode-line?
  (unless crypto-use-mode-line
    (setq mode-line-format nil))
  ;; Set up header line
  (tabulated-list-init-header))

(defvar crypto-mode-map (make-sparse-keymap))
(define-key crypto-mode-map "n" 'crypto-next)
(define-key crypto-mode-map "p" 'crypto-previous)
(define-key crypto-mode-map "g" 'crypto-refresh)
(define-key crypto-mode-map "q" 'crypto-quit)


(defvar crypto-coinlist-file
  (when load-file-name
    (concat (file-name-directory load-file-name)
            "coinlist"))
  "Path to the coinlist file.")

(defun crypto-is-crypto-p (coin)
  "Return non-nil if COIN exists."
  (with-temp-buffer
    (insert-file-contents crypto-coinlist-file)
    (save-excursion
      (save-match-data
	(re-search-forward (format "^%s$" coin) nil t)))))

(defun crypto-get-price (coin)
  "Return the current price of COIN.
If COIN is not a cryptocurrency, then parse the price database file
and return its price."
  (if (crypto-is-crypto-p coin)
      (crypto-parse-json coin "PRICE")
    (crypto-parse-pricedb coin)))

(defun crypto-parse-pricedb (symb)
  "Parse the price database file and return the last price of SYMB.
Throw an error if neither SYMB nor pricedb file is found."
  (if (file-exists-p crypto-pricedb-file)
      (with-temp-buffer
	(insert-file-contents crypto-pricedb-file)
	(save-excursion
	  (save-match-data
	    (goto-char (point-max))
	    (if (re-search-backward (format "^P.+\s%s\s[A-Z\.$€£₹_(]?\\([0-9\.]+\\)" symb) nil t)
		(string-to-number (match-string-no-properties 1))
	      (user-error "%S not found" symb)))))
    (user-error "%S not found" crypto-pricedb-file)))

(defun crypto-get-amount (coin)
  "Return the number of COINs."
  (cdr (assoc coin crypto-holdings)))

(defun crypto-holding-p (coin)
  "Return non-nil if COIN is member of `crypto-holdings'."
  (member coin (mapcar #'car crypto-holdings)))

(defun crypto-24h-percent-change (coin)
  "Return the percent change of COIN in the last 24 hours."
  (if (crypto-is-crypto-p coin)
      (format "%0.2f%%" (crypto-parse-json coin "CHANGEPCT24HOUR")) ""))

(defun crypto-24h-price-change (coin)
  "Return the price change of COIN in the last 24 hours."
  (if (crypto-is-crypto-p coin)
      (format "%0.2f" (crypto-parse-json coin "CHANGE24HOUR")) ""))

(defun crypto-get-value (coin)
  "Return COIN price times the number of coins."
  (* (crypto-get-price coin)
     (crypto-get-amount coin)))

(defun crypto-noncrypto-assets ()
  "Return a list of all non-crypto assets in the journal."
  (let (noncrypto)
    (mapc (lambda (symb)
	    (unless (crypto-is-crypto-p symb)
	      (push symb noncrypto)))
	  (mapc #'car crypto-holdings))
    noncrypto))

(defun crypto-assets ()
  "Return comma separated list of crypto assets."
  (let ((coins (--> (mapcar #'car crypto-holdings)
		    (-difference it (crypto-noncrypto-assets)))))
    (mapconcat 'identity coins ",")))

(defun crypto-fetch-json ()
  "Get cryptocurrency data from cryptocompare."
  (let* ((coins (crypto-assets))
	 (url-request-method "GET")
	 (url (format "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=%s&tsyms=%s,BTC"
		      coins crypto-default-currency-code))
	 (url-cache-expire-time 10)	;expires after 10 secs
	 (url-automatic-caching t))
    (if (url-cache-expired url)
    	(-when-let (buffer (url-retrieve-synchronously url t t))
    	  (with-current-buffer buffer
    	    (-when-let (fname (url-cache-create-filename url))
    	      (when (url-cache-prepare fname)
    		(let ((coding-system-for-write 'utf-8))
    		  (write-region (point-min) (point-max) fname nil 5)))))
    	  buffer)
      (url-fetch-from-cache url))))

(defun crypto-read-json ()
  "Return the JSON object of coins."
  (let ((json-key-type 'string))
    (with-current-buffer
	(crypto-fetch-json)
      (goto-char (point-min))
      (re-search-forward "^$")
      (json-read))))

(defun crypto-parse-json (coin prop)
  "Return the PROP value of COIN."
  (when (crypto-is-crypto-p coin)
    (--> (assoc "RAW" (crypto-read-json))
	 (assoc coin it)
	 (assoc-default prop (-flatten it)))))

(defun crypto-price-in-btc (coin)
  "Return price of COIN in BTC."
  (if (crypto-is-crypto-p coin)
      (--> (assoc "RAW" (crypto-read-json))
	   (assoc coin it)
	   (assoc "BTC" it)
	   (assoc-default "PRICE" (-flatten it)))
    (* (crypto-get-amount coin)
       (/ (crypto-get-price coin)
	  (crypto-get-price "BTC")))))

(defun crypto-value-in-btc (coin)
  "Return the value of COIN in BTC.
Result is the price in BTC times the amount held."
  (let ((btc-price (crypto-price-in-btc coin))
	(amount (crypto-get-amount coin)))
    (* btc-price amount)))

(defun crypto-group-number (num &optional size char)
  "Format NUM as string grouped to SIZE with CHAR."
  ;; Based on code for `math-group-float' in calc-ext.el
  (let* ((size (or size 3))
	 (char (or char ","))
	 (str (if (stringp num)
		  num
		(number-to-string num)))
	 ;; omitting any trailing non-digit chars
	 ;; NOTE: Calc supports BASE up to 36 (26 letters and 10 digits ;)
	 (pt (or (string-match "[^0-9a-zA-Z]" str) (length str))))
    (while (> pt size)
      (setq str (concat (substring str 0 (- pt size))
			char
			(substring str (- pt size)))
	    pt (- pt size)))
    str))

(defun crypto-round-number (num)
  "Round NUM greater than one to two decimal places.
Otherwise, round it to four decimal places. This is needed for coins
under 1 cent."
  (if (> num 1)
      (format "%0.02f" num)
    (format "%0.04f" num)))

(defmacro crypto-format-number (&rest body)
  "Group and round number before executing BODY."
  `(crypto-group-number (crypto-round-number ,@body)))

(defun crypto-total ()
  "Add value of coins and return the total."
  (apply '+ (mapcar (lambda (holding)
		      (crypto-get-value (car holding)))
		    crypto-holdings)))

(defun crypto-get-weight (coin)
  "Return the percentage of the portfolio that COIN occupies."
  (let ((value (* (crypto-get-price coin)
		  (crypto-get-amount coin)))
	(total (crypto-total)))
    (* (/ value total) 100)))

(defun crypto-net-gain (coin)
  "Calculate the net profit/loss of COIN."
  (let ((entry-price (cdr (assoc coin crypto-entry-price)))
	(current-price (crypto-get-price coin)))
    (* (/ (- current-price entry-price) entry-price) 100)))

(defun crypto-collect-entry-price (coin)
  "Collect the acquisition cost of COIN."
  (with-temp-buffer
    (insert-file-contents crypto-ledger-file)
    (let (matches)
      (save-excursion
	(save-match-data
	  (goto-char (point-min))
	  (while (re-search-forward
		  ;; The syntax could be @ $100.00 or {=$100.00}
		  (format "[0-9\.]+\s%s\s{?=?@?\s?[A-Z\.$€£₹_(]?+\s*\\([0-9\.,]+\\)" coin) nil t)
	    (push (replace-regexp-in-string "," "" (match-string-no-properties 1)) matches))
	  matches)))))

(defun crypto-calculate-entry-price (coin)
  "Calculate the entry price of COIN."
  (-when-let* ((costs (crypto-collect-entry-price coin))
	       (count (length costs)))
    (cons coin
	  (/ (apply '+ (mapcar #'string-to-number costs))
	     count))))

(defmacro crypto-with-fontification (str &rest prop)
  "Print STR using PROP."
  `(propertize ,str 'face (list ,@prop)))

(defmacro crypto-fontify (num)
  "Fontify numbers differently.
If NUM is negative, make it red. Green otherwise."
  `(crypto-with-fontification
    ,num (if (string-match-p "-" ,num)
	     'crypto-negative-number-face
	   'crypto-positive-number-face)))

(defun crypto-total-in-btc ()
  "Add value of coins in BTC and return the total."
  (apply '+ (mapcar (lambda (holding)
		      (crypto-value-in-btc (car holding)))
		    crypto-holdings)))

(defun crypto-collect-holdings ()
  "Parse journal and populate `crypto-holdings'."
  (let ((case-fold-search nil)
	(amount)
	(coin)
	(coins)
	(holding))
    (with-temp-buffer
      (insert-file-contents crypto-ledger-file)
      (save-excursion
	(save-match-data
	  (goto-char (point-min))
	  (while (re-search-forward
		  "^[\s\ta-zA-Z:\s]*\\([-0-9\.]+\\)\s\\([A-Z]*\\)\s?" (point-max) t)
	    (let ((account (match-string-no-properties 0))
		  (symb (match-string-no-properties 2))
		  (quantity (match-string-no-properties 1)))
	      (setq coin symb)

	      ;; This is very ugly, but basically if coin amount is an
	      ;; expense, then we have to subtract it from the total.
	      ;; If it's an income we have to add it to the total.
	      (if (string-match "Expenses?\\|expenses?" account)
		  (setq amount (string-to-number (concat "-" quantity)))
		(if (string-match "Income\\|income" account)
		    (setq amount (string-to-number (replace-regexp-in-string "-" "" quantity)))
		  (setq amount (string-to-number quantity))))

	      (unless (member coin crypto-ignore)
		(cl-pushnew coin coins :test 'equal)
		(push (cons coin amount) holding))))))
      (setq crypto-holdings
	    (mapcar (lambda (coin)
		      (cons coin
			    (apply '+ (mapcar (lambda (c)
						(if (string= (car c) coin)
						    (cdr c)
						  0))
					      holding))))
		    coins)))))


(defvar crypto-ledger-hash nil)
(defvar crypto-cached-entry-price nil)

(defun crypto-entry-price ()
  "Parse journal and return a list of entry prices."
  (-when-let (file crypto-ledger-file)
    (with-temp-buffer
      (insert-file-contents file)
      ;; Check hash and reparse if necessary
      (let ((ledger-hash (secure-hash 'sha256 (current-buffer))))
	(unless (and crypto-cached-entry-price
		     (string= crypto-ledger-hash ledger-hash))
	  ;; Populate crypto-holdings before building the alist
	  (crypto-collect-holdings)
	  (let ((entry-price (mapcar (lambda (coin)
				       ;; Throw error if entry price is not found
				       (-if-let (ep (crypto-calculate-entry-price coin))
				       	   ep (user-error "Entry price not found")))
				     (mapcar #'car crypto-holdings))))
	    (setq crypto-cached-entry-price entry-price))
	  (setq crypto-ledger-hash ledger-hash))
	crypto-cached-entry-price))))

(defun crypto-file-modified-p ()
  "Return t if `crypto-ledger-file' was changed."
  (-when-let (file crypto-ledger-file)
    (with-temp-buffer
      (insert-file-contents file)
      (let ((ledger-hash (secure-hash 'sha256 (current-buffer))))
	(not (string= crypto-ledger-hash ledger-hash))))))


;;; Hash table and formatters

(defvar crypto-hashtable (make-hash-table :test 'equal)
  "Hash table for crypto assets.")

(defun crypto-hashtable-mismatch-p ()
  "Return t if cached coins are different from those in `crypto-holdings'."
  ;; This checks if a coin was added or removed. If the two lists are
  ;; different then we need to rerun the `crypto-create-table'
  ;; function with the clear-cache argument.
  (not (-same-items?
	(hash-table-keys crypto-hashtable)
	(mapcar #'car crypto-holdings))))

(defun crypto-gethash (coin)
  "Return list of values associated with COIN."
  (gethash coin crypto-hashtable))

(defun crypto-read-property (coin prop)
  "For COIN read PROP."
  (assoc-default prop (crypto-gethash coin)))

(defun crypto-format-coin (coin)
  "Return a list of COIN values using `crypto-hashtable'.
The element has the form (COIN [COIN VALUE AMOUNT WEIGHT PROFIT])."
  (let ((sign crypto-default-currency-sign)
	(value (crypto-format-number (crypto-read-property coin :value)))
	(amount (format "%0.8f" (crypto-read-property coin :amount)))
	(weight (format "%0.01f%%" (crypto-read-property coin :weight)))
	(profit (format "%0.0f%%" (crypto-read-property coin :profit))))
    (list coin (vector coin (concat sign value) amount weight profit))))

(defun crypto-total-profit ()
  "Return the total profit/loss in percentage points."
  (apply '+ (mapcar (lambda (coin)
		      (/ (* (crypto-read-property coin :profit)
			    (crypto-read-property coin :weight))
			 100))
		    (mapcar #'car crypto-holdings))))

(defun crypto-format-total ()
  "Return a list with the total value of the portfolio.
The element has the form (ID [ID VALUE VALUE-IN-BTC nil TOTAL-PROFIT])."
  (let* ((sign crypto-default-currency-sign)
	 (coin (caar crypto-holdings))
	 (total (crypto-format-number (crypto-read-property coin :total)))
	 (total-in-btc (format "%0.8f" (crypto-read-property coin :total-in-btc)))
	 (total-profit (format "%0.0f%%" (crypto-total-profit))))
    (list "Total" (vector "Total" (concat sign total) total-in-btc "-" total-profit))))

(defun crypto-tabulated-list ()
  "Return a tabulated list of all coins in the portfolio.
Each element has the form (ID [ID VAL1 ... VAL4])."
  (let ((coins (-remove (lambda (coin)
			  ;; coins are only considered if the amount
			  ;; is not 0.
			  (= (crypto-read-property coin :amount) (or 0.0 0)))
			(mapcar #'car crypto-holdings))))
    (cons (crypto-format-total)
	  (mapcar #'crypto-format-coin coins))))

(defun crypto-create-table (&optional clear-cache)
  "Create hashtable for coins.
With optional argument CLEAR-CACHE, empty cache and recalculate
values."
  (if (or (hash-table-empty-p crypto-hashtable)
	  (crypto-hashtable-mismatch-p)
  	  (crypto-file-modified-p)
  	  clear-cache)
      (progn
	(message "Checking...")
	(let ((total (crypto-total))
	      (total-in-btc (crypto-total-in-btc)))
	  (setq crypto-cached-entry-price nil) ;empty cache
	  (setq crypto-hashtable (make-hash-table :test 'equal)) ;empty hash table
	  (cl-loop for holding in (if crypto-ledger-file
				      (setq crypto-holdings (crypto-collect-holdings))
				    crypto-holdings)
		   for coin = (car holding)
		   for price = (crypto-get-price coin)
		   for price-in-btc = (crypto-price-in-btc coin)
		   for change = (crypto-24h-percent-change coin)
		   for price-change = (crypto-24h-price-change coin)
		   for value = (crypto-get-value coin)
		   for amount = (crypto-get-amount coin)
		   for weight = (crypto-get-weight coin)
		   for profit = (crypto-net-gain coin)
		   do (puthash coin `((:price        . ,price)
				      (:price-in-btc . ,price-in-btc)
				      (:change       . ,change)
				      (:price-change . ,price-change)
				      (:value        . ,value)
				      (:amount       . ,amount)
				      (:weight       . ,weight)
				      (:profit       . ,profit)
				      (:total        . ,total)
				      (:total-in-btc . ,total-in-btc))
			       crypto-hashtable))
	  (crypto-tabulated-list)))
    (crypto-tabulated-list)))

;; Align and format table

(defun crypto-align-numbers ()
  "Align numbers on whitespace and decimal points."
  (let ((indent-tabs-mode nil)
	(sign (regexp-quote crypto-default-currency-sign)))
    (align-regexp (point-min) (point-max) (format "\\(\\s-*\\)\\(\s[-0-9,%s]+\\)\\.?" sign)
		  -2 0 t)))

(defun crypto-clean-table ()
  "Clean table by removing blank lines and whitespace."
  (save-excursion
    (goto-char (point-max))
    (while (looking-back "^$\\|\s" nil)
      (delete-char -1))
    (goto-char (point-min))
    (end-of-line)
    (while (looking-back "\s" nil)
      (delete-char -1))))

(defun crypto-format-table ()
  "Align and clean table."
  (crypto-align-numbers)
  (crypto-clean-table))

(defun crypto-default-position ()
  "Move cursor to the beginning of the first row and display message."
  (goto-char (point-min))
  ;; when crypto-use-header-line is nil, we need the point past
  ;; the fake header line.
  (unless tabulated-list-use-header-line
    (forward-line))
  (forward-line)
  (crypto-previous))

(defun crypto-minibuffer-message ()
  "Display additional information in the minibuffer.
Include price and percent change in the last 24 hours of coin at
point."
  (when (crypto-holding-p (thing-at-point 'symbol t))
    (let* ((coin (thing-at-point 'symbol t))
	   (sign crypto-default-currency-sign)
	   (price (crypto-format-number (crypto-read-property coin :price)))
	   (change (crypto-fontify (crypto-read-property coin :change)))
	   (price-change (crypto-read-property coin :price-change))
	   (price-change (if (string= "" price-change) ""
			   (crypto-fontify
			    (concat "(" sign price-change ")")))))
      (message "%s %s%s %s %s"
	       coin
	       sign
	       price
	       change
	       price-change))))


;;;###autoload
(defun crypto-next ()
  "Move cursor down and display message."
  (interactive)
  (let ((message-log-max nil))
    (unless (equal (point) (line-beginning-position))
      (beginning-of-line))
    ;; Do not move forward if cursor is at the beginning of the last
    ;; line
    (unless (save-excursion
	      (end-of-visual-line)
	      (when (equal (point) (point-max))
		t))
      (forward-line))
    (when (equal (thing-at-point 'word t) "Total")
      (forward-line -1))
    (crypto-minibuffer-message)))

;;;###autoload
(defun crypto-previous ()
  "Move cursor up and display message."
  (interactive)
  (let ((message-log-max nil))
    (unless (equal (point) (line-beginning-position))
      (beginning-of-line))
    (unless (or (bobp)
		;; The faker header id should be nil, so if that's the
		;; case stop the cursor from moving up
		(not (save-excursion
		       (forward-line -1)
		       (tabulated-list-get-id))))
      (forward-line -1))
    (crypto-minibuffer-message)))

;;;###autoload
(defun crypto-refresh ()
  "Rebuild buffer display."
  (interactive)
  (with-current-buffer crypto-buffer-name
    (crypto-mode)
    (let ((inhibit-read-only t))
      (setq tabulated-list-entries
	    (crypto-create-table t))
      (tabulated-list-print t)
      (crypto-format-table)
      (crypto-default-position)
      (hl-line-mode 1))))

;;;###autoload
(defun crypto-quit ()
  "Kill portfolio buffer and delete its window."
  (interactive)
  (let ((window (get-buffer-window crypto-buffer-name)))
    (kill-buffer crypto-buffer-name)
    (when (and window (not (one-window-p window)))
      (delete-window window))))

;;;###autoload
(defun cryptopf (arg)
  "Display crypto portfolio.
With a prefix ARG, clear cache and recompute values."
  (interactive "P")
  (when crypto-ledger-file
    (setq crypto-entry-price (crypto-entry-price)))
  (with-current-buffer
      (get-buffer-create crypto-buffer-name)
    (pop-to-buffer crypto-buffer-name)
    (let ((inhibit-read-only t))
      (crypto-mode)
      (setq tabulated-list-entries
	    (crypto-create-table (when arg t)))
      (tabulated-list-print t)
      (crypto-format-table)
      (fit-window-to-buffer)
      (crypto-default-position)
      (hl-line-mode 1))))

(provide 'cryptopf)
;;; cryptopf.el ends here
